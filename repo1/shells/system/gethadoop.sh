#!/bin/bash

#wget http://ftp.piotrkosoft.net/pub/mirrors/ftp.apache.org/hadoop/common/hadoop-2.6.4/hadoop-2.6.4.tar.gz
sudo mv /home/vagrant/system/hadoop-2.6.4.tar.gz /usr/local/
cd /usr/local/
sudo tar -xzf hadoop-2.6.4.tar.gz
sudo mv hadoop-2.6.4 hadoop
sudo chown vagrant:hadoop -R /usr/local/hadoop

sudo mv /home/vagrant/etc/* /usr/local/hadoop/etc/hadoop/
