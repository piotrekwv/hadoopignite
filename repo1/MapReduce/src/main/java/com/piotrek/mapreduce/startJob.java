/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piotrek.mapreduce;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.*;

/**
 *
 * @author piotrek
 */
public class startJob extends HttpServlet implements Tool {

    private Configuration conf;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet start</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Job initiated</h1>");
            out.println("</body>");
            out.println("</html>");
            int status = ToolRunner.run(new Configuration(), new startJob(), new String[0]);
     
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(startJob.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(startJob.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public int run(String[] strings) throws Exception {
       this.setConf(conf);
   
        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(new Path("output"))) {
            fs.delete(new Path("output"), true);
        }
        Job job = Job.getInstance(conf);
        job.setJobName("Entropy");
        job.setJarByClass(this.getClass());
       
        job.setMapperClass(MapperEntropy.class);
        job.setReducerClass(ReducerEntropy.class);
        job.setSortComparatorClass(SortEntropy.class);//SortEntropy.class
       
        job.setOutputKeyClass(DoubleWritable.class);
        job.setOutputValueClass(Text.class);

       
        org.apache.hadoop.mapreduce.lib.input.FileInputFormat.addInputPath(job, new Path("input"));
        org.apache.hadoop.mapreduce.lib.output.FileOutputFormat.setOutputPath(job, new Path("output"));
       
        return job.waitForCompletion(true) ? 0 : -1;
    }

    @Override
    public void setConf(Configuration c) {
      conf = new Configuration();
        System.setProperty("HADOOP_USER_NAME", "vagrant");
        conf.set("mapred.job.tracker", "192.168.5.10:8021");
        conf.set("fs.default.name", "hdfs://192.168.5.10:9000/user/vagrant");
        conf.set("hadoop.job.ugi", "vagrant");

    }

    @Override
    public Configuration getConf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
