package com.piotrek.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;



public class MapperEntropy extends Mapper<Object, Text, DoubleWritable, Text> {

    private final static IntWritable zero = new IntWritable(0);
    private Text word = new Text();
   
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
        double val = val(value);
        context.write(new DoubleWritable(val), value);
    }
    
    private static double val(Text key){
        String line = key.toString();
        String abc = "[a-z]";
        String ABC = "[A-Z]";
        String num = "[0-9]";
        String oth = "[^a-zA-Z0-9]";
        double n = 0.0;
        double result;
        Pattern letter = Pattern.compile(abc);
        Pattern capital = Pattern.compile(ABC);
        Pattern number = Pattern.compile(num);
        Pattern other = Pattern.compile(oth);
        Matcher l = letter.matcher(line);
        Matcher c = capital.matcher(line);
        Matcher nu = number.matcher(line);
        Matcher o = other.matcher(line);
        if (l.find()) n += 26.0;
        if (c.find()) n += 26.0;
        if (nu.find()) n += 10.0;
        if (o.find()) n += 32.0;
        result=line.length() * (Math.log(n) / Math.log(2));
        return result; 
    }
   
}
