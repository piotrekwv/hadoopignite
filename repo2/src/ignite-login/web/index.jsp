<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String user = null;
    if(session.getAttribute("username")!=null)
        user = (String) session.getAttribute("username");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login system</title>
    </head>
    <body>
        <h1>Log in</h1>
       
        <%
            if(user != null) {
                %>
                Logged as <%=user%><br/>
                <%
            }
        %>
        <c:choose>
            <c:when test="<%=user!=null%>">
                <h3>Welcome <strong><%=user%>!</strong></h3>
                <a href="logout">Logout!</a>
            </c:when>
            <c:otherwise>
                <h3>Please enter username and password</h3>
                <form action="login" method="POST">
                    Username:<input type="text" name="username"/><br/>
                    Password:<input type="password" name="password"/><br/>
                    <input type="submit" value="OK"/>
                </form>
            </c:otherwise>
        </c:choose>
    </body>
</html>
