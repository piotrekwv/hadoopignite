#!/bin/bash
# make sure you are in directory with Vagrantfile

#get hadoop installation archive
wd=`pwd`
cd files/resources
if [ ! -e apache-ignite-fabric-1.5.0.final-bin.zip ]; then
	echo "Downloading ignite 1.5"
	wget http://ftp.ps.pl/pub/apache//ignite/1.5.0.final/apache-ignite-fabric-1.5.0.final-bin.zip
fi
cd -
#generate ssh key for passwordless connection
if [ ! -e ~/.ssh/id_rsa.pub ]; then
	echo "Generating keys and copying"
	ssh-keygen -P '' -t dsa -f files/system/id_rsa
	cp id_rsa* ~/.ssh/
else
	echo "Copying keys"
	cp ~/.ssh/id_rsa* files/system/
fi



