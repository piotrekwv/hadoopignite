#!/bin/bash

sudo apt-get install tomcat7 -y
rm -rf /var/lib/tomcat7/webapps/*
mkdir /var/lib/tomcat7/webapps_2

chown tomcat7:tomcat7 /var/lib/tomcat7/webapps_2
chown tomcat7:tomcat7 /etc/tomcat7

cp /home/vagrant/resources/server.xml /var/lib/tomcat7/conf/

cp /home/vagrant/resources/ignite-login.war /var/lib/tomcat7/webapps/ROOT.war
cp /home/vagrant/resources/ignite-login.war /var/lib/tomcat7/webapps_2/ROOT.war

sudo service tomcat7 restart

