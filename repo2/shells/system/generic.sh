#!/bin/bash

sudo mv /home/vagrant/system/bashrc /home/vagrant/.bashrc
sudo mv /home/vagrant/system/hosts /etc/hosts
sudo mv /home/vagrant/system/config /home/vagrant/.ssh/config
sudo mv /home/vagrant/system/cluster /etc/dsh/group/cluster
sudo mv /home/vagrant/system/slaves /etc/dsh/group/slaves
